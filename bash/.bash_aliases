# Basic commands
alias la='ls -A'
alias l='ls -CF'
alias ll='ls -alF'
alias lt='du -sh * | sort -h'

# Color
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# System commands replacement
alias cat='batcat'
alias du='gdu'

# Note Taking
alias obsidian='exec /home/brice/Obsidian-0.15.9.AppImage'

# System
alias sysup='sudo apt-get update && sudo apt-get upgrade && sudo apt-get dist-upgrade'
alias install='sudo apt-get install'
alias purge='sudo apt-get purge'
alias search='sudo apt-cache search'

# Cybersecurity
alias armitage='cd /home/brice/CyberSecurity_Ressources/C2_Framework/Armitage/release/unix/ && bash armitage && cd ~'
alias armitage_server=$'cd /home/brice/CyberSecurity_Ressources/C2_Framework/Armitage/release/unix/ && sudo ./teamserver `ip addr show tun0 | grep -m 1 inet | awk \'{print $2}\' | cut -d / -f 1` password'

# Tmux
alias ..='cd ..'
alias tast='tmux attach-session -t'
alias tns='tmux new -s'
alias tls='tmux list-session'

# Personal Script
# Remove in git*

# Virtual Machines
alias create_img='qemu-img create -f qcow2 '
alias create_kvm='kvm -hda virtualdebian.img -m 2048 -net nic -net user -soundhw all -cdrom '
alias start_kvm='kvm -soundhw all -m 2048 -hda '

# Other
alias a='sudo systemctl restart lightdm.service'
alias gh='history|grep'
alias untar='tar -zxvf'
alias left='ls -t -1 -l'
alias count_files='find . -type f | wc -l'
alias cpv='rsync -ah --info=progress2'
alias mnt="mount | awk -F' ' '{ printf \"%s\t%s\n\",\$1,\$3; }' | column -t | egrep ^/dev/ | sort"
alias lse="ls -lF --color=always | grep --color=never '*$'"
alias lsd='ls -d */'
alias lsf="ls -plL --color=always | grep -v '^d'"
