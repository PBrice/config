"========================================="
"General Settings"
"========================================="
syntax on
set ma
set mouse=a
set cursorline
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set autoread
set nobackup
set nowritebackup
set noswapfile
set nu
set foldlevelstart=99
set foldmethod=indent
set scrolloff=7
set clipboard=unnamedplus

"========================================="
"Vim-Plug Automatic Installation"
"========================================="
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"========================================="
"Plugins"
"========================================="
call plug#begin('~/.config/nvim/plugged')
" Color scheme
" dracula/vim, phanviet/vim-monokai-pro rhysd/vim-color-spring-night, arzg/vim-colors-xcode, kyoz/purify
Plug 'arzg/vim-colors-xcode'
Plug 'kyoz/purify', { 'rtp': 'vim'}

" Autocomplete plugin
" Also do :CocInstall coc-clangd coc-tsserver coc-eslint coc-json coc-prettier coc-css coc-python coc-java 
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Nvim motions
Plug 'phaazon/hop.nvim'

" Plug for Go(lang) programming language
Plug 'fatih/vim-go', {'do': ':GoUpdateBinaries'}

" Telescope => Files finder
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

" Telescope => native, coc
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
Plug 'fannheyward/telescope-coc.nvim'

" Telescope Devicons
Plug 'kyazdani42/nvim-web-devicons'

" Nvim Devicons
Plug 'ryanoasis/vim-devicons'

call plug#end()
"
"========================================="
"Plugins Settings"
"========================================="
" Aesthetic
colorscheme xcodedarkhc
" colorscheme purify
set background=dark

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

"==============================================================================
"Key combos
"==============================================================================
set encoding=UTF-8
let mapleader = " "

" Navigate buffers
nnoremap <leader>bn :bnext<CR>
nnoremap <leader>bp :bprevious<CR>
nnoremap <leader>bf :bfirst<CR>
nnoremap <leader>bl :blast<CR>

" Other
nnoremap <leader><CR> :source ~/.config/nvim/init.vim<CR>
nnoremap <leader>ne :Telescope file_browser<CR>
nnoremap <leader>f :call CocAction('format')<CR>
nmap <silent> rn <Plug>(coc-rename)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
nmap <silent> cld :CocList diagnostics<CR>

autocmd Filetype go command! -bang AV call go#alternate#Switch(<bang>0, 'vsplit')
autocmd Filetype go command! -bang AS call go#alternate#Switch(<bang>0, 'split')
autocmd StdinReadPre * let s:std

" COC
" <tab> for trigger completion and navigate to the next complete item
function! s:check_back_space() abort
      let col = col('.') - 1
        return !col || getline('.')[col - 1]  =~ '\s'
    endfunction

    inoremap <silent><expr> <Tab>
          \ pumvisible() ? "\<C-n>" :
          \ <SID>check_back_space() ? "\<Tab>" :
          \ coc#refresh()
    
" Vimwiki conflicts with coc autocompletion, disable tab for vimwiki
let g:vimwiki_table_mappings = 0
" Or 
" au filetype vimwiki silent! iunmap <buffer> <Tab>

"========================================="
"Development Settings"
"========================================="
" GO Language
augroup auto_go
    autocmd!
"    autocmd BufWritePost *.go :GoBuild
"    autocmd BufWritePost *_test.go :GoTest
augroup end
